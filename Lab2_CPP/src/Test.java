import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Test {
        private String name;
        private int year;
        private int printing;// тираж
        private String frequency;// періодичність, частота видання
        private int edition;//видавництво

        public Test(String s_name, int s_year, int s_printing, String s_frequency, int s_edition) {
                name = s_name;
                year = s_year;
                printing = s_printing;
                edition = s_edition;
                frequency = s_frequency;
        }


        public static void main(String[] args) {

                Test[] newTest = new Test[10];
                newTest[0] = new Test("New York Times", 1999, 1500, "weekly", 1);
                newTest[1] = new Test("The Times", 2000, 1000, "monthly", 2);
                newTest[2] = new Test("The City", 1995, 1000, "weekly", 3);
                newTest[3] = new Test("Morning", 2000, 1200, "monthly", 4);
                newTest[4] = new Test("The Washington Post", 1995, 1500, "weekly", 5);
                newTest[5] = new Test("The Independent", 2000, 1200, "monthly", 6);
                newTest[6] = new Test("The Daily Telegraph", 1995, 150, "weekly", 7);
                newTest[7] = new Test("Positive News", 2000, 1250, "monthly", 8);
                newTest[8] = new Test("People", 1995, 800, "weekly", 9);
                newTest[9] = new Test("Daily Mail", 1995, 800, "weekly", 10);

//------------------------------------------------------------------------------------------

                        //створення карти, занесення даних і виведення її не екран
                        HashMap<Integer, String> myHashMap = new HashMap<Integer, String>();//ключ(видавництво), значення  - назва
                HashMap<Integer, String> myNewHashMap = new HashMap<Integer, String>();//ключ(видавництво), значення  - назва

                        for (int i = 0; i < 10; i++)
                                myHashMap.put(newTest[i].edition, newTest[i].name);



//---------------------------------------------------------------------------------
                while(true) {
                        System.out.println("\n\n1 - Print map\n2 - Removing\n3 - Frequency response\n4 - Swapping\n" +
                                "5 - Read from file 1\n6 - Read from fle 2\n7 - Sorting by name reversed ");
                        System.out.println("Enter your choice:");
                        Scanner scan = new Scanner(System.in);
                        String answ = scan.nextLine();

//----------------------------------------------------------------------------------
                        if(Integer.parseInt(answ) == 1) {
                                System.out.println("Standard:\n");
                                for (Map.Entry<Integer, String> entry : myHashMap.entrySet())
                                        System.out.println(entry.getKey() + " - " + entry.getValue());
                        }
//-------------------------------------------------------------------------------------------
                       else  if(Integer.parseInt(answ) == 2) {
                                //видалити
                                System.out.println("Removing:\n");
                                for (int i = 0; i < 3; i++) {
                                        if (newTest[i].printing < 900) {
                                                String del = newTest[i].name;
                                                for (Map.Entry<Integer, String> entry : myHashMap.entrySet()) {
                                                        if (del.equals(entry.getValue()))
                                                                myHashMap.remove(entry.getKey());
                                                }

                                        }
                                }
                                for (Map.Entry<Integer, String> entry : myHashMap.entrySet())
                                        System.out.println(entry.getKey() + " - " + entry.getValue());
                        }


//------------------------------------------------------------------------------------------
                        else if(Integer.parseInt(answ) == 3) {
                                //частотна характеристика різних тиражів
                                int k = 10;
                                int[] arr = new int[k];
                                int[] arr2 = new int[k];
                                for (int i = 0; i < k; i++) {
                                        arr[i] = 0;
                                        arr2[i] = 0;
                                }
                                arr[0] = newTest[0].printing;
                                arr2[0] = 1;
                                for (int i = 1; i < k; i++) {
                                        for (int index = 0; index < k; index++) {
                                                if (newTest[i].printing == arr[index]) {
                                                        arr2[index] += 1;
                                                        break;
                                                } else {
                                                        index++;
                                                        if (index < k && arr2[index] == 0) {
                                                                arr[index] = newTest[i].printing;
                                                                arr2[index] += 1;
                                                                index--;
                                                                break;
                                                        } else index--;
                                                }

                                        }

                                }

                                for (int i = 0; i < k; i++) {
                                        if (arr2[i] != 0)
                                                System.out.println(arr[i] + " - " + arr2[i]);
                                }
                        }

//---------------------------------------------------------------------------------------------
                        else if(Integer.parseInt(answ) == 4) {
                                //сортування у прямому порядку відносно ключа
                                 System.out.println("Sorting by key for swapping:\n");
                                myHashMap.entrySet().stream()
                                        .sorted(Map.Entry.<Integer, String>comparingByKey())
                                        .forEach(System.out::println);
                                //поміняти місцями парні і непарні елементи
                                for (int i = 0; i < 9; i++) {
                                        if ((float) i % 2 == 0) {
                                                int kk = i++;
                                                String temp = newTest[i].name;
                                                newTest[i].name = newTest[kk].name;
                                                newTest[kk].name = temp;
                                        }
                                }

                                HashMap<Integer, String> myHashMap2 = new HashMap<Integer, String>();//ключ(видавництво), значення  - назва


                                for (int i = 0; i < 10; i++)
                                        myHashMap2.put(newTest[i].edition, newTest[i].name);
                                System.out.println("Swapping:\n");
                                for (Map.Entry<Integer, String> entry : myHashMap2.entrySet())
                                        System.out.println(entry.getKey() + " - " + entry.getValue());
                        }


//-------------------------------------------------------------------------------------------------
                        else  if(Integer.parseInt(answ) == 5) {
                                //зчитування з файлу першого набору
                                try {
                                        File file1 = new File("C:/lab/note1.txt");
                                        //создаем объект FileReader для объекта File
                                        FileReader fr1 = new FileReader(file1);
                                        //создаем BufferedReader с существующего FileReader для построчного считывания
                                        BufferedReader reader1 = new BufferedReader(fr1);
                                        // считаем сначала первую строку
                                        String line1 = reader1.readLine();
                                        System.out.println("Read from file 1:\n");
                                        while (line1 != null) {
                                                System.out.println(line1);
                                                // считываем остальные строки в цикле
                                                String[] subStr1;
                                                String delimeter1 = " "; // Разделитель
                                                subStr1 = line1.split(delimeter1);

                                                // for(int i = 0; i < subStr1.length; i++)
                                                //System.out.println(subStr[i]);
                                                line1 = reader1.readLine();
                                                myNewHashMap.put(Integer.parseInt(subStr1[4]), subStr1[0]);
                                        }
                                } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                } catch (IOException e) {
                                        e.printStackTrace();
                                }
                        }

//----------------------------------------------------------------------------------------
                        else  if(Integer.parseInt(answ) == 6) {
                                //зчитування з файлу другого набору
                                try {
                                        File file2 = new File("C:/lab/note2.txt");
                                        //создаем объект FileReader для объекта File
                                        FileReader fr2 = new FileReader(file2);
                                        //создаем BufferedReader с существующего FileReader для построчного считывания
                                        BufferedReader reader2 = new BufferedReader(fr2);
                                        // считаем сначала первую строку
                                        String line2 = reader2.readLine();
                                        System.out.println("Read from file 2:\n");
                                        while (line2 != null) {
                                                System.out.println(line2);
                                                // считываем остальные строки в цикле
                                                String[] subStr2;
                                                String delimeter2 = " "; // Разделитель
                                                subStr2 = line2.split(delimeter2);
                                                //for(int i = 0; i < subStr2.length; i++)
                                                //System.out.println(subStr[i]);
                                                line2 = reader2.readLine();
                                                myNewHashMap.put(Integer.parseInt(subStr2[4]), subStr2[0]);

                                        }
                                } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                } catch (IOException e) {
                                        e.printStackTrace();
                                }
                        }

//---------------------------------------------------------------------------------
                        else  if(Integer.parseInt(answ) == 7) {
                                //сортування у зворотньому порядку відносно назви

                                System.out.println("Create one map from two and sorting by name reversed:\n");
                                myNewHashMap.entrySet().stream()
                                        .sorted(Map.Entry.<Integer, String>comparingByValue().reversed())
                                        .forEach(System.out::println);
                        }

//--------------------------------------------------------------------------------------------

                }





 //------------------------------------------------------------------------------
        }


}
