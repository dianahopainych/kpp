import java.util.regex.*;
import java.io.*;
import java.util.*;
import java.io.File;



class DirFilter implements FilenameFilter {
    private Pattern pattern;

    public DirFilter(String regex) {

        pattern = Pattern.compile("."+regex);

    }

    public boolean accept(File dir, String name) {

        return pattern.matcher(name).matches();

    }

}
//-------------------------------------------------------

class MyThread implements Runnable {



    public void run(){

        System.out.printf("%s started... \n", Thread.currentThread().getName());
        String tt = "MyThread started";


        File dir = new File("D:/Git");

        File[] children = dir.listFiles();
        System.out.println("List of files:");
        for (File file : children) {
            System.out.println(file.getAbsolutePath());
        }

        System.out.println("\n-----------------------");

        MyThread myThread2 = new MyThread();
        Thread t1 = new Thread(myThread2,"MyThread2");
        System.out.printf(tt+"\n");

        System.out.println("List of files bigget than 20 000 bytes:");

        String[] paths = dir.list();

        // for (String path : paths) {
        for(int i=0; i< paths.length; i++){
            if(children[i].length() > 20000)
                System.out.println(paths[i]+' '+ children[i].length());
        }


        //int count = dir.list().length;
        //System.out.println(count);

        int cnt = 0;
        for(int i=0; i< paths.length; i++) {
            if (children[i].isDirectory())
                cnt++;
        }

        System.out.println("\n-----------------------");
        MyThread myThread3 = new MyThread();
        Thread t3 = new Thread(myThread3,"MyThread3");
        System.out.printf(tt);

        System.out.println("Number of directories:");
        System.out.println(cnt);
        System.out.println("\n-----------------------");
        MyThread myThread4 = new MyThread();
        Thread t4 = new Thread(myThread4,"MyThread4");
        System.out.printf(tt);

        System.out.println("Only directories:\n");
        for(int i=0; i< paths.length; i++){
            if(children[i].isDirectory())
                //if(children[i].length() > 20000)
                System.out.println(paths[i]+' '+ children[i].length());
        }


        MyThread myThread5 = new MyThread();
        Thread t5 = new Thread(myThread5,"MyThread5");
        System.out.printf(tt+"\n");
        String aa1 = "D:/ForLab4/";
        String aa2 = "*.txt";

        File path = new File(aa1);
        String[] list;

            list = path.list(new DirFilter(aa2));

        Arrays.sort(list, String.CASE_INSENSITIVE_ORDER);
        System.out.println("Files with pattern *.txt:");
        for (String dirItem : list)
            System.out.println(dirItem);


        //System.out.printf("%s finished... \n", Thread.currentThread().getName());
    }


}
//------------------------------------------------------------------
public class Main {

    public static void main(String[] args) {

        System.out.println("Main thread started...");
        MyThread myThread = new MyThread();
        Thread t = new Thread(myThread,"MyThread");
        t.start();


        System.out.println("Main thread finished...");
    }

}




