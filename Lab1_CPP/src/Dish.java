import java.util.*;

public class Dish implements Comparable<Dish> {

    private int id;
    private String name;
    private String category;
    public char c;
    private long price;

    public int getId() {

        return id;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
  }

    public long getPrice() {
        return price;
    }

    public Dish(int id, String name, String category, int price) {
        this.id = id;
        this.name = name;
        this.category = category;
        if(this.category == "classic")
            this.c = 'c';
        if (this.category == "kids")
            this.c = 'k';
        if(this.category == "vegetarian")
            this.c = 'v';
        this.price = price;
    }

    @Override
    public int compareTo(Dish emp) {
        return (this.id - emp.id);
    }
//searching

    public static int[] compilePatternArray(String pattern) {
        int patternLength = pattern.length();
        int len = 0;
        int i = 1;
        int[] compliedPatternArray = new int[patternLength];
        compliedPatternArray[0] = 0;

        while (i < patternLength) {
            if (pattern.charAt(i) == pattern.charAt(len)) {
                len++;
                compliedPatternArray[i] = len;
                i++;
            } else {
                if (len != 0) {
                    len = compliedPatternArray[len - 1];
                } else {
                    compliedPatternArray[i] = len;
                    i++;
                }
            }
        }
       // System.out.println("Compiled Pattern Array " + Arrays.toString(compliedPatternArray));
        return compliedPatternArray;
    }


    public static ArrayList<Integer> performKMPSearch(String text, String pattern) {
        int[] compliedPatternArray = compilePatternArray(pattern);

        int textIndex = 0;
        int patternIndex = 0;

        ArrayList<Integer> foundIndexes = new ArrayList<>();

        while (textIndex < text.length()) {
            if (pattern.charAt(patternIndex) == text.charAt(textIndex)) {
                patternIndex++;
                textIndex++;
            }
            if (patternIndex == pattern.length()) {
                foundIndexes.add(textIndex - patternIndex);
                patternIndex = compliedPatternArray[patternIndex - 1];
            }

            else if (textIndex < text.length() && pattern.charAt(patternIndex) != text.charAt(textIndex)) {
                if (patternIndex != 0)
                    patternIndex = compliedPatternArray[patternIndex - 1];
                else
                    textIndex = textIndex + 1;
            }
        }
        return foundIndexes;
    }

    @Override
    public String toString() {
        return "[id=" + this.id + ", name=" + this.name + ", category=" + this.category + ", price=" + this.price + "]";
    }

    public static Comparator<Dish> PriceComparator = new Comparator<Dish>() {

        @Override
        public int compare(Dish e1, Dish e2) {
            return (int) (e1.getPrice() - e2.getPrice());
        }
    };

    public static Comparator<Dish> CategoryComparator = new Comparator<Dish>() {

      @Override
    public int compare(Dish e1, Dish e2) {
          return e1.getCategory().compareTo(e2.getCategory());
       }
  };


    public static Comparator<Dish> NameComparator = new Comparator<Dish>() {

        @Override
        public int compare(Dish e1, Dish e2) {
            return e1.getName().compareTo(e2.getName());
        }
    };
//static inner class
    //*************************************************************
    public static class CompByNameD implements Comparator<Dish> {

        @Override
        public int compare(Dish e1, Dish e2) {
            return -e1.getName().compareTo(e2.getName());
        }

    }


}

