import java.util.*;

public class main {

    public static void main(String[] args) {

        Dish[] empArr = new Dish[4];
        empArr[0] = new Dish(10, "chocolate desert", "kids", 100);
        empArr[1] = new Dish(20, "vegetable soup", "vegetarian", 80);
        empArr[2] = new Dish(5, "salat", "classic", 50);
        empArr[3] = new Dish(1, "beaf", "classic", 120);

        String symbols = Character.toString(empArr[0].c) + Character.toString(empArr[1].c)
                + Character.toString(empArr[2].c)+ Character.toString(empArr[3].c);
        System.out.println("classic - c, kids - k, vegetarian - v");
        System.out.println("Enter category:");

        Scanner scan = new Scanner(System.in);
        String pat = scan.nextLine();

        ArrayList<Integer> indexes = Dish.performKMPSearch(symbols, pat);
        System.out.println("Search:");
        for(int i = 0; i < indexes.size(); i++)
            System.out.println(empArr[indexes.get(i)]);
        Arrays.sort(empArr);
        System.out.println("Standard:\n"+Arrays.toString(empArr));

        Arrays.sort(empArr, Dish.PriceComparator);
        System.out.println("Comparator to Price:\n"+Arrays.toString(empArr));


        Arrays.sort(empArr, Dish.CategoryComparator);
        System.out.println("Comparator to Category:\n"+Arrays.toString(empArr));

        Arrays.sort(empArr, Dish.NameComparator);
        System.out.println(" Comparator to Name:\n"+Arrays.toString(empArr));

        empArr[0] = new Dish(1, "milk shake", "kids", 75);
        Arrays.sort(empArr, new CompByIdAndName());
        System.out.println("Comparator to Id and Name:\n"+Arrays.toString(empArr));

        Arrays.sort(empArr, new Dish.CompByNameD());
        System.out.println("Comparator to name D:\n"+Arrays.toString(empArr));
       //anonumous comparator
        Arrays.sort(empArr, new Comparator<Dish>() {
            @Override
            public int compare(Dish o1,  Dish o2) {
                return (int) (-(o1.getPrice() - o2.getPrice()));
            }
        });
        System.out.println("Anonymous Comparator to price D :\n"+Arrays.toString(empArr));

        //lambda expressions
        Arrays.sort(empArr, (m1, m2) -> (-(m1.compareTo(m2))));
        System.out.println("Comparator with lambda expressions to index D :\n"+Arrays.toString(empArr));
    }

}