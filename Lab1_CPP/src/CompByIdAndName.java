import java.util.Comparator;

public class CompByIdAndName implements Comparator<Dish> {

    @Override
    public int compare(Dish o1, Dish o2) {

        int flag = o1.getId() - o2.getId();

        if(flag == 0) flag = o1.getName().compareTo(o2.getName());
        return flag;
    }
}